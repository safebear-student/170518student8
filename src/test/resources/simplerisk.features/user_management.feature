Feature: User Management

  User Story:
  In order to manage users
  As an administrator
  I need CRUD permissions to user accounts

  Rules:
  - Non admin users have no permission over other accounts
  - There are two types of non-admin accounts
  - Risk managers
  - Asset managers
  - The administrators must be able to reset user's passwords

  Questions:
  - Do admin users need access to all accounts or only to the accounts in their group?
  To do:
  - Force reset of password on account creation

  Domain Language:

  Group = Users are defined by which group they belong to
  CRUD = Create, Read, Update, Delete
  administrator permissions = access to CRUD risks, users and assets for all groups
  risk_management permissions = only able to CRUD risks
  asset_management permissions = only able to CRUD assets

  Background:
    Given a user called simon with administrator permissions and password S@feb3ar
    And a user called tom with risk_management permissions and password S@feb3ar

    @high-impact
    Scenario Outline: The administrator checks a user's details
      When simon is logged in with password <password>
      Then he is able to view <user>'s account
      Examples:
        | password | user |
        | S@feb3ar | tom  |

    @high-risk
    @to-do
    Scenario Outline:   A user's password is reset by the administrator
      Given a <user> has lost his password
      When simon is logged in with password S@feb3ar
      Then simon can reset <user>'s password
      Examples:
        | user |
        | tom  |






